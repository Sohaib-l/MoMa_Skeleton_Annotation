#ifndef __SequenceView__
#define __SequenceView__

#include <iostream>

namespace MoMa {

	class TracksView;
}

#include "mmCanvas.h"

class ofApp;
#include "ofApp.h"

namespace MoMa {

	class TracksView : public MoMa::Canvas {

	public:

		TracksView(ofApp *app, MoMa::Position position = DEFAULT, MoMa::Position
			alignment = MoMa::DEFAULT, MoMa::Canvas *parentUI = NULL, bool minified = false);

		void canvasEvent(ofxUIEventArgs &e);

		void update(void);

		ofApp *app;
	};
}

#endif
