#include "tracksView.h"

using namespace MoMa;
struct {

	bool operator()(MoMa::Label a, MoMa::Label b) {

		return a.moment.time() < b.moment.time();
	}
} customLess2;

TracksView::TracksView(ofApp *_app, MoMa::Position position, MoMa::Position alignment, MoMa::Canvas *relative, bool minified) :
	app(_app),
	MoMa::Canvas(_app, "Joints", position, alignment, relative, NULL, NULL, minified)

{
	rect->setWidth(30);
	getCanvasTitle()->getRect()->setWidth(30 - 10);
	getCanvasTitle()->calculatePaddingRect();

	addSpacer();
	for (int i = 0; i < app->names_of_joints.size(); i++) {
		addToggle(app->names_of_joints[i], false, 15, 15);
		setWidgetPosition(OFX_UI_WIDGET_POSITION_RIGHT);
		addToggle(app->names_of_joints[i] + "_trace", false, 15, 15);
		setWidgetPosition(OFX_UI_WIDGET_POSITION_DOWN);
	}
	
	addSpacer();
	addButton("Select All", false);
	addButton("Unselect All", false);
	addButton("Mirror", false);

	addSpacer();
	addLabel("Other Features");
	addToggle("handR-handL", false, 15, 15);

	addSpacer();
	addButton("Validate", false);
	addSpacer();
	addButton("Save", false);
	addSpacer();
	addLabel("Count", "0");
	addButton("Clear", false);

	initCanvas();
}

void TracksView::update() {


}

void TracksView::canvasEvent(ofxUIEventArgs &e) {

	string name = e.getName();
	if (app->mouse_pressed) {

		if (name == "Validate") {

			app->is_validate = true;

			vector<float> tmp_values = vector<float>();
			for (int i = 0; i < app->names_of_joints.size(); i++) {
				ofxUIToggle *toggle = (ofxUIToggle *)getWidget(app->names_of_joints[i]);
				if (toggle->getValue()) tmp_values.push_back(1.f);
				else tmp_values.push_back(0.f);
			}
			app->values.push_back(tmp_values);

			ofxUIButton * button = (ofxUIButton *)getWidget("Validate");
			button->setColorFill(ofColor(255, 255, 255));
			app->warning_text = "";
		}

		for (int i = 0; i < app->names_of_joints.size(); i++) {

			if (name == app->names_of_joints[i]) {

				ofxUIButton * button = (ofxUIButton *)getWidget("Validate");
				button->getLabelWidget()->setColorFill(Red);
				app->warning_text = "Validate";

				ofxUIToggle *toggle = (ofxUIToggle *)getWidget(app->names_of_joints[i]);
				app->selected_joints[i] = toggle->getValue();
			}

			if (name == app->names_of_joints[i] + "_trace") {
				
				ofxUIToggle *toggle = (ofxUIToggle *)getWidget(app->names_of_joints[i] + "_trace");
				app->selected_traces[i] = toggle->getValue();
			}

		}

		if (name == "Save" && app->is_validate) {

			/*ofFileDialogResult res;
			res = ofSystemSaveDialog("preset.csv", "Saving Labels to File");
			if (res.bSuccess) {*/
				app->mocap_file_name = app->mocap_file_name.substr(0, app->mocap_file_name.size()-4);
				std::sort(app->labelList("Track1").begin(), app->labelList("Track1").end(), customLess2);
				//labelList("Track1").save(res.filePath);
				cout << "creating csv" << endl;
				arma::mat matrix_gt = arma::zeros(app->track("Track1").nOfFrames(), 21);
				app->printl(to_string(matrix_gt.n_rows) + " " + to_string(matrix_gt.n_cols));

				int v = 0;
				for (int i = 0; i < app->labelList("Track1").size(); i += 2) {
					for (int j = 0; j < app->values[v].size(); j++) {
						if (app->values[v][j] > 0) {
							matrix_gt(arma::span(app->labelList("Track1")[i].moment.index(), app->labelList("Track1")[i + 1].moment.index()), arma::span(j, j)) =
								arma::ones(app->labelList("Track1")[i + 1].moment.index() - app->labelList("Track1")[i].moment.index() + 1, 1);
						}
					}
					v++;
				}
				cout << "saving csv" << endl;

				matrix_gt.save(app->mocap_file_name + "_gt.csv", arma::csv_ascii);
				app->printl(to_string(matrix_gt.n_rows) + " " + to_string(matrix_gt.n_cols));
				cout << "creating the image" << endl;
				ofImage im;
				im.allocate(matrix_gt.n_rows, matrix_gt.n_cols, OF_IMAGE_GRAYSCALE);
				for (int i = 0; i < matrix_gt.n_rows; i++)
				{
					for (int j = 0; j < matrix_gt.n_cols; j++)
					{
						im.setColor(i, j, ofFloatColor(matrix_gt(i, j)));
					}
				}
				im.update();
				im.resize(224, 224);
				cout << "saving the image" << endl;
				im.save(app->mocap_file_name + "_gt.jpg");
			//}
				app->count++;
				ofxUILabel *label = (ofxUILabel *)getWidget("Count");
				label->setLabel(to_string(app->count));
				app->is_validate = false;
		}

		if (name == "Select All") {
			for (int i = 0; i < app->names_of_joints.size(); i++) {

				ofxUIToggle *toggle = (ofxUIToggle *)getWidget(app->names_of_joints[i]);
				toggle->setValue(true);

				ofxUIButton * button = (ofxUIButton *)getWidget("Validate");
				button->getLabelWidget()->setColorFill(Red);
				app->warning_text = "Validate";
				app->selected_joints[i] = toggle->getValue();
			}
		}

		if (name == "Unselect All") {
			for (int i = 0; i < app->names_of_joints.size(); i++) {

				ofxUIToggle *toggle = (ofxUIToggle *)getWidget(app->names_of_joints[i]);
				toggle->setValue(false);

				ofxUIButton * button = (ofxUIButton *)getWidget("Validate");
				button->getLabelWidget()->setColorFill(Red);
				app->warning_text = "Validate";
				app->selected_joints[i] = toggle->getValue();
			}
		}

		if (name == "Mirror") {
			vector<bool> tmp = vector<bool>(app->selected_joints.size());
			for (int i = 0; i < tmp.size(); i++) {

				if(i < 5) tmp[i] = app->selected_joints[i];
				else tmp[i] = false;
			}

			for (int i = 0; i < app->names_of_joints.size(); i++) {

				if (app->selected_joints[i]) {

					if (i == 5) tmp[9] = true;
					else if (i == 6) tmp[10] = true;
					else if (i == 7) tmp[11] = true;
					else if (i == 8) tmp[12] = true;
					else if (i == 9) tmp[5] = true;
					else if (i == 10) tmp[6] = true;
					else if (i == 11) tmp[7] = true;
					else if (i == 12) tmp[8] = true;
					else if (i == 13) tmp[17] = true;
					else if (i == 14) tmp[18] = true;
					else if (i == 15) tmp[19] = true;
					else if (i == 16) tmp[20] = true;
					else if (i == 17) tmp[13] = true;
					else if (i == 18) tmp[14] = true;
					else if (i == 19) tmp[15] = true;
					else if (i == 20) tmp[16] = true;
				}
			}

			app->selected_joints = tmp;
			for (int i = 0; i < app->names_of_joints.size(); i++) {
				
				ofxUIToggle *toggle = (ofxUIToggle *)getWidget(app->names_of_joints[i]);
				toggle->setValue(app->selected_joints[i]);
			}
			ofxUIButton * button = (ofxUIButton *)getWidget("Validate");
			button->getLabelWidget()->setColorFill(Red);
			app->warning_text = "Validate";
		}

		if (name == "Clear") {

			app->count = 0;
			ofxUILabel *label = (ofxUILabel *)getWidget("Count");
			label->setLabel(to_string(app->count));
		}

		if (name == "handR-handL") {

			ofxUIToggle *toggle = (ofxUIToggle *)getWidget("handR-handL");
			app->draw_handR_handL = toggle->getValue();
		}


		app->mouse_pressed = false;
	}


}
