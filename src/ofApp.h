/**
 *
 *  @file ofApp.h
 *  @brief MotionMachine header file for empty example
 *  @copyright Numediart Institute, UMONS (c) 2015
 *
 */
#pragma once
#include "MoMa.h"
#include "MoMaUI.h"
#include "ofMain.h"
#include "tracksView.h"
#include "mmMultiTimedFlatParser.h"
#include "mmGeometry.h"

class ofApp : public MoMa::SceneApp {
  public:
    
    void setup( void );
    void update( void );
    
    void scene3d( void );
    void scene2d( void );
    void annotate( void );
    
    void keyPressed( int key );
    void keyReleased( int key );
    
    void mousePressed( int x, int y, int button );
    void mouseReleased( int x, int y, int button );
    void mouseDragged( int x, int y, int button );
    void mouseMoved( int x, int y );
    
    void windowResized( int w, int h );
    void dragEvent( ofDragInfo dragInfo );
    void gotMessage( ofMessage msg );
	bool has_suffix(const std::string &str, const std::string &suffix);
	void print(string message);
	void printl(string message);
	inline bool exists(const std::string& name);

	MoMa::TracksView *tracksview;
	
	vector<vector<float>> values;
	bool mouse_pressed;
	vector<string> names_of_joints;
	string warning_text;

	ofImage my_image;
	ofVideoPlayer video_player;
	string mocap_path;

	bool loading_test;
	string mocap_file_name;
	vector<MoMa::Track> tracks;

	vector<MoMa::Track> split_track(MoMa::Track tr);
	vector<bool> selected_joints;
	vector<bool> selected_traces;
	unsigned int count;
	bool is_validate;

	// Other features
	arma::vec handR_handL;
	bool draw_handR_handL;
};
