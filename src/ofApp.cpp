/**
 *
 *  @file ofApp.cpp
 *  @brief MotionMachine source file for empty example
 *  @copyright Numediart Institute, UMONS (c) 2015
 *
 */
#include "ofApp.h"
using namespace std;
using namespace arma;
using namespace MoMa;

struct {

	bool operator()(MoMa::Label a, MoMa::Label b) {

		return a.moment.time() < b.moment.time();
	}
} customLess;

void ofApp::setup( void ) {
	
	mocap_path = "D:/Sohaib/datasets/NTU/2_ntu_dataset_txt";
	is_validate = false;

	addNewTrack("Track1"); // Dynamically add new track
	registerDragEvent(track("Track1")); // Expose to d&d

	addNewLabelList("Track1"); // Dynamically add new label list
	registerDragEvent(labelList("Track1")); // Expose to d&d
	track("Track1").setFrameRate(30);
	track("Track1").load(getDataPath() + "my_bones.bones");
	track("Track1").load(getDataPath() + "my_nodes.nodes");

	ofSetFrameRate(30);
	setFrameRate(30);

	showNodeNames(true);
	
	tracks = vector<Track>();
	warning_text = "";

	names_of_joints = vector<string>();
	names_of_joints.push_back("head");
	names_of_joints.push_back("neck");
	names_of_joints.push_back("spineS");
	names_of_joints.push_back("spineM");
	names_of_joints.push_back("spineB");
	names_of_joints.push_back("shoulR");
	names_of_joints.push_back("elbowR");
	names_of_joints.push_back("wristR");
	names_of_joints.push_back("handR");
	names_of_joints.push_back("shoulL");
	names_of_joints.push_back("elbowL");
	names_of_joints.push_back("wristL");
	names_of_joints.push_back("handL");
	names_of_joints.push_back("hipR");
	names_of_joints.push_back("kneeR");
	names_of_joints.push_back("ankR");
	names_of_joints.push_back("footR");
	names_of_joints.push_back("hipL");
	names_of_joints.push_back("kneeL");
	names_of_joints.push_back("ankL");
	names_of_joints.push_back("footL");

	// Affichage fenetre
	tracksview = new MoMa::TracksView(this, LEFT);
	values = vector<vector<float>>();

	mouse_pressed = false;
	loading_test = false;
	selected_joints = vector<bool>(names_of_joints.size());
	selected_traces = vector<bool>(names_of_joints.size());
	count = 0;

	// Other features
	draw_handR_handL = false;

}
void ofApp::update( void ) {
    
}
void ofApp::scene3d( void ) {
    
	if (track("Track1").nOfFrames() > 0) {

		Frame frame = track("Track1")[getAppIndex()];
		ofSetColor(Turquoise);
		setNodeSize(20);
		draw(frame);

		ofSetColor(255, 215, 0);
		setNodeSize(60);
		for (int i = 0; i < track("Track1").nOfNodes(); i++) {
			if (selected_joints[i]) {

				Node node = frame(i);
				draw(node);
			}
		}
		setNodeSize(DefaultNodeSize);

		ofPushStyle();

		if (loading_test && my_image.getHeight()>0) {

			for (int i = 0; i < track("Track1").nOfNodes(); i++) {
				if (my_image.getColor(getAppIndex(), i).r > 150) {
					
					ofSetColor(255, 0, 80, 150);
					setNodeSize((float)my_image.getColor(getAppIndex(), i).r/255*100);
					Node node = frame(i);
					draw(node);
				}
				setNodeSize(DefaultNodeSize);
			}
			setNodeSize(DefaultNodeSize);
		}
		ofPopStyle();
	}	
}
void ofApp::scene2d( void ) {
    
	if (track("Track1").nOfFrames() > 0) {

		for (int i = 0; i < selected_traces.size(); i++) {

			if (selected_traces[i]) {

				Trace trace = track("Track1")(i);
				draw(trace);
			}

		}
	}

	for (int i = 22; i < 21; i++) {

		ofLine(0, ofGetWindowHeight() / 21 * i, ofGetWindowWidth(), ofGetWindowHeight() / 21 * i);
	}

	if (labelList("Track1").size() >= 2 && values.size() > 0)
	{
		int v = 0;
		for (int i = 1; i < labelList("Track1").size(); i += 2) {

			for (int j = 0; j < 21; j++) {
				ofPushStyle();
				ofSetColor(Red, 100);
				if (values[v][j] > 0) {
					ofRect( labelList("Track1")[i-1].moment.index()*ofGetWindowWidth()/track("Track1").nOfFrames(),
						    ofGetWindowHeight()/21*j,
							labelList("Track1")[i].moment.index()*ofGetWindowWidth()/track("Track1").nOfFrames() -
							labelList("Track1")[i-1].moment.index()*ofGetWindowWidth()/track("Track1").nOfFrames(),
							ofGetWindowHeight()/21);
					ofPushStyle();
					ofSetColor(ofColor(255, 255, 255));
					ofDrawBitmapString(names_of_joints[j], 
									   labelList("Track1")[i - 1].moment.index()*ofGetWindowWidth() / track("Track1").nOfFrames()+10,
									   ofGetWindowHeight()/21*j+10);
					ofPopStyle();
				}
				ofPopStyle();
			}
			v++;
		}
	}
	ofPushStyle();
	ofSetColor(Red);
	ofDrawBitmapString(warning_text, ofGetMouseX(), ofGetMouseY());
	ofPopStyle();

	if (draw_handR_handL) {

		draw(handR_handL, "Distance between the two hands");
	}
}
void ofApp::annotate( void ) {
    
    
}
void ofApp::keyPressed( int key ) {

	if (key == 'f') {

		cout << "Index: " << getAppIndex() << endl;
		cout << "Time: " << getAppTime() << endl;
	}
    
	if (key == OF_KEY_INSERT) {

		if (activeMode == ANNOTATE) {

			if (labelList("moma").size() > 0) {

				selectedLabelIdx = labelList("moma").size() - 1;
				(*mouseEventRegLabelList).back().state = UNSELECTED;
			}

			labelList("moma").push_back(Label(Moment(getAppIndex(), frameRate), "new"));
			selectedLabelIdx = labelList("moma").size() - 1;
			(*mouseEventRegLabelList).back().state = SELECTED;
			isLabelSelected = true;
		}

	}
}
void ofApp::keyReleased( int key ) {
    
    
}
void ofApp::mousePressed( int x, int y, int button ) {
    
	mouse_pressed = true;
}
void ofApp::mouseReleased( int x, int y, int button ) {
    
}
void ofApp::mouseDragged( int x, int y, int button ) {
    
    
}
void ofApp::mouseMoved( int x, int y ) {

	ofDrawBitmapString(warning_text, x, y);

}

void ofApp::windowResized( int w, int h ) {
    

}
void ofApp::dragEvent( ofDragInfo dragInfo ) {

	for (int i = 0; i < dragInfo.files.size(); i++) {

		if (has_suffix(dragInfo.files[i], ".nodes") || has_suffix(dragInfo.files[i], ".bones")) {
			cout << "nodelist or bonelist!" << endl;
		}

		else if(has_suffix(dragInfo.files[i], ".txt")) {

			mocap_file_name = dragInfo.files[i];
			setPlayerSize(track("Track1").nOfFrames());
			track("Track1").setFrameRate(30);
			labelList("Track1").clear();
			ofSetFrameRate(30);
			setFrameRate(30);
			cout << "number of frames of this track: " << track("Track1").nOfFrames() << endl;
			cout << "number of nodes of this track: " << track("Track1").nOfNodes() << endl;
			values = vector<vector<float>>();

			// Other features
			handR_handL = vec(track("Track1").nOfFrames());
			for (int t = 0; t < track("Track1").nOfFrames(); t++) {

				mat slice = track("Track1").position.getData().slice(t);
				handR_handL(t) = Geometry::distance(slice.unsafe_col(7), slice.unsafe_col(11));
			}
			
		}

		else if (has_suffix(dragInfo.files[i], ".jpg") || has_suffix(dragInfo.files[i], ".png")) {

			if (loading_test) {

				my_image.load(dragInfo.files[i]);
				my_image.resize(track("Track1").nOfFrames(), track("Track1").nOfNodes());
			}
			else {

				mocap_file_name = dragInfo.files[i];
				mocap_file_name = mocap_file_name.substr(mocap_file_name.find_last_of("\\")+1, 20) + ".txt";
				string c = mocap_file_name.substr(mocap_file_name.find_last_of("A"), 4);
				mocap_file_name = mocap_path + "/" + c + "/" + mocap_file_name;
				cout << "mocap_file_name ==> " << mocap_file_name << endl;
				track("Track1").load(mocap_file_name);
				setPlayerSize(track("Track1").nOfFrames());
				track("Track1").setFrameRate(30);
				labelList("Track1").clear();
				ofSetFrameRate(30);
				setFrameRate(30);
				cout << "number of frames of this track: " << track("Track1").nOfFrames() << endl;
				cout << "number of nodes of this track: " << track("Track1").nOfNodes() << endl;
			}

		}
		else if (has_suffix(dragInfo.files[i], ".ttxt")) {

			track("Track1").load(dragInfo.files[i]);
			tracks = split_track(track("Track1"));

			setPlayerSize(track("Track1").nOfFrames());
			track("Track1").setFrameRate(30);
			labelList("Track1").clear();
			ofSetFrameRate(30);
			setFrameRate(30);
		}
	}
}
void ofApp::gotMessage( ofMessage msg ) {
    
    
}

bool ofApp::has_suffix(const std::string &str, const std::string &suffix)
{
	return str.size() >= suffix.size() &&
		str.compare(str.size() - suffix.size(), suffix.size(), suffix) == 0;
}

void ofApp::print(string message) {

	cout << message;
}

void ofApp::printl(string message) {

	cout << message << endl;
}

vector<MoMa::Track> ofApp::split_track(Track tr) {

	vector<MoMa::Track> tracks = vector<MoMa::Track>();
	cube datas = tr.position.getData();
	for (int i = 0; i < 6; i++) {

		cube d = datas(span::all, span(i * 25, (i + 1) * 25 - 1), span::all);
		cout << d.n_cols << endl;
		Track t;
		t.position.setData(30, d);
		tracks.push_back(t);
	}

	tr.setFrameRate(30);
	tracks.push_back(tr);
	return tracks;
}

inline bool ofApp::exists(const std::string& name) {

	struct stat buffer;
	return (stat(name.c_str(), &buffer) == 0);
}